& 0.0 & 0.2 & 0.6 & 1.0 & 3.0 & 10.0 \\
\hline
\multirow{3}{*}{0.0} & 11.64 \cellcolor{Periwinkle} & 11.65 \cellcolor{Periwinkle} & 11.66 \cellcolor{Periwinkle} & 11.29 \cellcolor{Periwinkle} & 7.91 & 5.45 \\
	& 11.67 & 11.68 & 11.66 & 11.31 & 7.84 & 5.15 \\
	& 12.22 & 12.22 & 11.93 & 11.30 & 7.80 \cellcolor{lightgray} & 5.08 \cellcolor{lightgray} \\
\hline
\multirow{3}{*}{0.2} & 11.65 \cellcolor{Periwinkle} & 11.65 \cellcolor{Periwinkle} & 11.66 & 11.63 & 9.66 & 7.61 \\
	& 11.72 & 11.70 & 11.66 \cellcolor{Salmon} & 11.61 \cellcolor{Salmon} & 9.77 & 7.78 \\
	& 12.22 & 12.22 & 11.99 & 11.64 & 9.64 \cellcolor{lightgray} & 7.58 \cellcolor{lightgray} \\
\hline
\multirow{3}{*}{0.5} & 11.65 \cellcolor{Periwinkle} & 11.66 \cellcolor{Periwinkle} & 11.64 \cellcolor{Periwinkle} & 11.66 & 10.53 & 8.97 \\
	& 11.71 & 11.70 & 11.67 & 11.64 \cellcolor{Salmon} & 10.68 & 9.15 \\
	& 12.22 & 12.23 & 12.14 & 11.84 & 10.45 \cellcolor{lightgray} & 8.92 \cellcolor{lightgray} \\
\hline
\multirow{3}{*}{1.0} & 11.66 \cellcolor{Periwinkle} & 11.65 \cellcolor{Periwinkle} & 11.64 \cellcolor{Periwinkle} & 11.64 \cellcolor{Periwinkle} & 11.06 & 9.99 \\
	& 11.68 & 11.71 & 11.71 & 11.65 & 11.25 & 10.22 \\
	& 12.25 & 12.26 & 12.22 & 12.00 & 10.99 \cellcolor{lightgray} & 9.93 \cellcolor{lightgray} \\
\hline
\multirow{3}{*}{2.0} & 11.65 \cellcolor{Periwinkle} & 11.67 \cellcolor{Periwinkle} & 11.65 \cellcolor{Periwinkle} & 11.67 & 11.45 & 10.82 \\
	& 11.73 & 11.67 & 11.69 & 11.67 \cellcolor{Salmon} & 11.57 & 11.00 \\
	& 12.27 & 12.25 & 12.22 & 12.18 & 11.42 \cellcolor{lightgray} & 10.72 \cellcolor{lightgray} \\
\hline

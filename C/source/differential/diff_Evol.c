/* differential/diff_Evol.c
 * 
 * Copyright (C) 2019 Abdellaziz Mohamed Amine, Zaoui Riyadh
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "../article.h"

//A program that uses a differential evolution to find the optimal variables for a maintenance problem
int
main (int argc, char *argv[])
{
  //The user needs to provide some arguments 
  if (argc < 7)
    {
      printf ("Not enough arguments. Usage:\n");
	printf(argv[0]);
	printf(" model pop_size nb_gen crossover_parameter differential_weight n file.params\n");
      return 1;
    }
  else
    {
      int model;
      int pop_size;
      int nb_gen;
      double diff_weight;
      double crossover;
      int n;

      sscanf (argv[1], "%d", &model);	//search the solutions for which model? 
      sscanf (argv[2], "%d", &pop_size);	//the number of individuals in each generation
      sscanf (argv[3], "%d", &nb_gen);	//the number of generations
      sscanf (argv[4], "%lf", &crossover);	//the crossover rate; between 0 and 1
      sscanf (argv[5], "%lf", &diff_weight);	//the differential weight; between 0.5 and 2
      sscanf (argv[6], "%d", &n);	//the number of cycles needed to compute the lowest accuracy of the fitness 

      //getting the parameters from a text file which path need to be provided by the user
      Params params = get_parameters (argv[7]);

      //the seed that will be used thoughout the whole program
      gsl_rng *r = gsl_rng_alloc (gsl_rng_mt19937);
      gsl_rng_set (r, time (NULL));

      Solution optimum;
      /* statistics components
      0 : nb_evaluated_individuals;
      1 : nb_since_almost_best;
      2 : nb_since_best;
      3 : nb_eval;
      4 : elapsed_time;
      */
      int statistics[5];
      diff_Evol (model, n, pop_size, nb_gen, crossover, diff_weight,
		 params, r, &optimum, statistics);
      printf
	("A Differential Evolution metaheuristic yielded the following optimal solution.\n");

      printf ("Parameters:\n\nmodel = %d\n", model);
      printf ("Size of a population: %d\n", pop_size);
      printf ("Nomber of generations: %d\n", nb_gen);
      printf ("Initial number of executions: %d\n\n", n);
      display (model, optimum);
      printf ("Total evaluations: %d, Evaluations since almost Best: %d, Evaluations since Best: %d\n",
        statistics[0], statistics[1], statistics[2]);
      printf ("Number of evaluations = %d\nElapsed time: %lf s\n", statistics[3],
        statistics[4]);

      gsl_rng_free (r);

      return 0;
    }
}

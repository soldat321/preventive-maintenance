/* comparison_models/comparison.c
 * 
 * Copyright (C) 2019 Abdellaziz Mohamed Amine, Zaoui Riyadh
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "../article.h"

//A function that prints a comparitive table in LaTeX format 
//of the best values for the three models 
int
main (int argc, char *argv[])
{
  gsl_rng *r = gsl_rng_alloc (gsl_rng_mt19937);
  gsl_rng_set (r, time (NULL));

  //The simulation parameters
  int n = 100;
  int nb_gen = 250;
  int pop_size = 40;

  //The matrices that will hold the best values found for each model
  gsl_matrix *imperfect_K = gsl_matrix_calloc (5, 6);
  gsl_matrix *treshold_T = gsl_matrix_calloc (5, 6);
  gsl_matrix *speed_S = gsl_matrix_calloc (5, 6);

  gsl_matrix *color = gsl_matrix_calloc (5, 6);

  //alpha and beta shall be set from the parameters file
  //whose path shall be entered by the user
  Params params;
  if (argc < 2)
    {
      params = get_parameters (NULL);
    }
  else
    {
      params = get_parameters (argv[1]);
    }

  //gamma and nu will vary 
  double gamma[5] = { 0.01, 0.2, 0.5, 1, 2 };
  double nu[6] = { 0, 0.2, 0.6, 1, 3, 10 };

  for (int i = 0; i < 5; i++)
    {
      for (int j = 0; j < 6; j++)
	{
	  params.gamu = gamma[i];
	  params.nu = nu[j];

	  Solution best;
      int *statistics = NULL;

	  //Searching the best solution for the first model
	  diff_Evol (1, n, pop_size, nb_gen, 0.2, 0.8, params, r, &best, statistics);
	  gsl_matrix_set (imperfect_K, i, j, best.fitness);

	  int color_buffer = 0;

	  //Searching the best solution for the second model
	  genetic (2, n, pop_size, nb_gen, 0.1, 0.95, params, r, &best, statistics);
	  gsl_matrix_set (treshold_T, i, j, best.fitness);

	  if (best.fitness < gsl_matrix_get (imperfect_K, i, j))
	    color_buffer = 1;

	  //Searching the best solution for the third model
	  genetic (3, n, pop_size, nb_gen, 0.2, 0.8, params, r, &best, statistics);
	  gsl_matrix_set (speed_S, i, j, best.fitness);

	  if (best.fitness < gsl_matrix_get (imperfect_K, i, j)
	      && best.fitness < gsl_matrix_get (treshold_T, i, j))
	    color_buffer = 2;

	  gsl_matrix_set (color, i, j, color_buffer);


	}
    }

  //Printing the table

  printf ("\t\\begin{table}\n");
  printf ("\t\\begin{adjustwidth}{0cm}{}\n");
  printf ("\\centering");
  printf ("\t\\begin{tabular}{ |c|c|c|c|c|c|c| } \n\n");

  printf ("\\hline\n");
  printf ("\t\\multicolumn{7}{|c|}{$(\\alpha = %.2lf, \\beta = %.2lf) $}\\\\\n",
	  params.alpha0, params.beta);
  printf ("\t\\hline \n");
  printf ("\t\\backslashbox{$\\gamma$}{$\\eta$}\n\n");

  for (int j = 0; j < 6; j++)
    printf ("& %.1lf ", nu[j]);
  printf ("\\\\\n");

  for (int i = 0; i < 5; i++)
    {
      printf ("\\hline\n");
      printf ("\\multirow{3}{*}{%.2lf} ", gamma[i]);


      for (int j = 0; j < 6; j++)
	{
	  int color_buffer = gsl_matrix_get (color, i, j);
	  printf ("& %.2lf ",
		  ceil (gsl_matrix_get (imperfect_K, i, j) * 100) / 100);
	  if (color_buffer == 0)
	    printf ("\\cellcolor{Periwinkle} ");
	}
      printf ("\\\\\n");

      printf ("\t");
      for (int j = 0; j < 6; j++)
	{
	  int color_buffer = gsl_matrix_get (color, i, j);
	  printf ("& %.2lf ",
		  ceil (gsl_matrix_get (treshold_T, i, j) * 100) / 100);
	  if (color_buffer == 1)
	    printf ("\\cellcolor{Salmon} ");
	}
      printf ("\\\\\n");

      printf ("\t");
      for (int j = 0; j < 6; j++)
	{
	  int color_buffer = gsl_matrix_get (color, i, j);
	  printf ("& %.2lf ",
		  ceil (gsl_matrix_get (speed_S, i, j) * 100) / 100);
	  if (color_buffer == 2)
	    printf ("\\cellcolor{lightgray} ");
	}
      printf ("\\\\\n");
    }
  printf ("\\hline\n\n");

  printf ("\\end{tabular}\n");
  printf ("  \\end{adjustwidth}\n");
  printf ("\\end{table}\n");

  gsl_rng_free (r);
  gsl_matrix_free (imperfect_K);
  gsl_matrix_free (treshold_T);
  gsl_matrix_free (speed_S);
  gsl_matrix_free (color);

  return 0;
}

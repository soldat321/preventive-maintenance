/* simulation/simulation.c
 * 
 * Copyright (C) 2019 Abdellaziz Mohamed Amine, Zaoui Riyadh
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "..//article.h"

//This program simulates the cost of a maintenance strategy
int main(int argc, char **argv)
{
	//The user need to provide some arguments 
    if(argc < 6)
    {
      printf ("Not enought arguments. Usage:\n");
	printf(argv[0]);
	printf(" model M K Q n file.params\n");
	return 1;
    }
    else
    {
	int model;
	Solution our_variables;
	int n;

	sscanf(argv[1],"%d", &model);	//which model to simulate; an integer ranging from 1 to 4
	
	//the three decision variables
	sscanf(argv[2],"%lf", &our_variables.M);	
	sscanf(argv[3],"%lf", &our_variables.K);
	sscanf(argv[4],"%lf", &our_variables.Q);
	
	//number of cycles to run in order to approximate the cost
	//the more cycles the more accuracy
	sscanf(argv[5],"%d", &n);

	//getting the parameters from a text file which path need to be provided by the user
	Params params = get_parameters(argv[6]);

	//the seed that will be used thoughout the whole program
	gsl_rng *r = gsl_rng_alloc(gsl_rng_mt19937);
	gsl_rng_set(r, time(NULL));
	

	objectif(model, &our_variables , n, params, r);
	printf("Estimating the cost per unit of time of a maintenance strategy.\n");
	display(model, our_variables);

	gsl_rng_free(r);
	return 0;
    }
}

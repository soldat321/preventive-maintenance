#include <stdio.h>
#include <time.h>
#include <math.h>
//#include <stdbool.h>
//#include "cdflib.h"
#include <gsl/gsl_math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_sort_vector.h>
#include <gsl/gsl_blas.h>

//A structure for holding the parameters of the problem, namely
//the random distributions parameters and the operations costs
struct Params;
typedef struct Params
{

//The degradation distribution parameters
  double alpha0;
  double beta;

  //The degradation speed random distribution parameter
  double gamu;
  //The imperfect action cost exponent parameter
  double nu;

  //The failure threshold
  double L;

  //The operations costs
  double Ci;			//inspection cost
  double Cc;			//corrective maintenance cost
  double Cp;			//perfect maintenance action cost
  double Cp0;			//the cost of an optimal imperfect maintennace action 
  double Cd;			//the downtime cost per unit of time 

} Params;

//A structure that holds the parameters of a solution
struct Solution;
typedef struct Solution
{
  double M;			//the maintenance threshold
  double K;			//the second parameter depends on the model
  double Q;			//the risk of failure rate
  double fitness;		//the policy cost per unit of time
} Solution;

//A structure that holds the boundaries of the search space for the metaheuristics
typedef struct Limits
{
  double inf_M;
  double sup_M;

  double inf_K;
  double sup_K;

  double inf_Q;
  double sup_Q;
} Limits;



//A function that returns the parameters of the maintenance problem
Params get_parameters (char *filename);

//A function that sets the boundaries of the search space
Limits set_limits (int model, double L);

//A function that initializes the Best individual by allocating it the very first individual of the population
void initialize_Best (int model, gsl_matrix * matrix, gsl_vector * Best,
		      int n, Params params, gsl_rng * r);


//A function for generating the first generation
void initialization_pop (int model, gsl_matrix * matrix, Limits limits,
			 gsl_rng * r);

//A function that computes a low accuracy of the fitness of a given individual in a population
void evaluation_individual (int model, gsl_matrix * matrix, int position,
			    int n, Params params, gsl_rng * r);


//A function that compares the fitness of an individual from the population with a another individual (most often the Best)
int comparison_individual (int model, gsl_matrix * matrix, int position,
			   gsl_vector * compared_with, int n, Params params,
			   gsl_rng * r);

//The birth process of a new individual in the Diffrentila Evolution function
void new_born (gsl_matrix * parent_matrix, gsl_matrix * child_matrix,
	       Limits limits, double crossover, double diff_weigh,
	       int position, gsl_rng * r);

//The BLX-alpha crossover function used in the genetic function
void genetic_crossover (int model, gsl_matrix * matrix, int position1,
			int position2, double alpha, int n,
			Params params, gsl_rng * r);

//A function that sets the positions of the two parents in the Genetic Algorithm
void get_positions_for_crossover (gsl_matrix * matrix, int *position1,
				  int *position2, gsl_rng * r);

//A function that executes the mutation part of the Genetic Algorithm
void genetic_mutation (int model, gsl_matrix * matrix, Limits limits,
		       double taux_mutation, gsl_rng * r);

//A function to displays a solution
void display (int model, Solution variables);

//A function that generates a random number according to a truncated Gaussian distribution
double trunc_gauss (double lower_bound, double upper_bound, gsl_rng * r);

//A function that returns the time to the next inspection time
double m (double X, double Q, double alpha_k, Params params);

//A function that simulates a cycle for the first model 
double cycle_number_imperfect (double M, int K, double Q, double *H,
			       Params params, gsl_rng * r);

//A function that simulates a cycle for the second model 
double cycle_seuil (double M, double K, double Q, double *H, Params params,
		    gsl_rng * r);

//A function that simulates a cycle for the third model 
double cycle_speed_estimate (double M, double S, double Q, double *H,
			     Params params, gsl_rng * r);

//A function that simulates a cycle for the fourth model 
double cycle_speed_exact (double M, double S, double Q, double *H,
			  Params params, gsl_rng * r);

//A function that computes/approximates the fitness (unit cost) of an individual
void objectif (int model, Solution * variables, int n, Params params,
	       gsl_rng * r);

//A function that readjusts an infeasible solution into a feasible one
void adjust (gsl_vector * v, Limits limits);

//A crossover function for the differential evolution algorithm
void crossover_differential (gsl_vector * mutant, gsl_vector * target,
            double CR, gsl_rng * r);

//An implementation of a genetic algorithm in order to search for the best solution (decision variables)
//for a given model and parameters
void genetic (int model, int n, int pop_size, int nb_gen,
	      double taux_croisement, double taux_mutation, Params params,
	      gsl_rng * r, Solution * optimum, int * statistics);

//An implementation of a differential evolution metaheuristic in order to search for the best solution (decision variables)
//for a given model and parameters
void diff_Evol (int model, int n, int pop_size, int nb_gen, double crossover,
	   double diff_weight, Params params, gsl_rng * r,
	   Solution * optimum, int *statistics);


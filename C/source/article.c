#include "article.h"

//A function that returns the parameters of the maintenance problem
Params
get_parameters (char *filename)
{
  FILE *pointer_file;

  pointer_file = fopen (filename, "r");
  Params params;
  if (pointer_file == NULL)
    {
      /* Fallback prameters. They describe the problem exposed in the following article.

         Phuc Do, Alexandre Voisin, Eric Levrat, Benoit Iung. "A proactive
         condition-based maintenance strategy with both perfect
         and imperfect maintenance actions". Reliability Engineering and System Safety,
         RESS 133 (2015) 22-32.

       */
      printf ("Error while loading file. using default parameters.\n");
      params.alpha0 = 1;
      params.beta = 1;

      params.gamu = 0.2;
      params.nu = 3;
      params.L = 20;
      params.Ci = 10;
      params.Cc = 100;
      params.Cp = 90;
      params.Cp0 = 70;
      params.Cd = 20;

    }
  else
    {

      char buffer[256];
      fgets (buffer, 256, pointer_file);
      sscanf (buffer, "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf", &params.alpha0,
	      &params.beta, &params.gamu, &params.nu, &params.L, &params.Ci,
	      &params.Cc, &params.Cp, &params.Cp0, &params.Cd);
      fclose (pointer_file);

    }



  return params;
}

//A function that sets the boundaries of the search space
Limits
set_limits (int model, double L)
{
  //Default boundaries
  Limits limits = { 0, L, 0, 20, 0.01, 0.99 };

  //Verifiying which model is used
  switch (model)
    {
    case 1:
      //Number of imperfect actions
      limits.inf_K = 0;
      limits.sup_K = 20;
      break;
    case 2:
      //Threshold model
      limits.inf_K = 0;
      limits.sup_K = L;
      break;
    case 3:
      //Approximated speed model
    case 4:
      //Exact speed model
      limits.inf_K = 0;
      limits.sup_K = 3;
      break;
    }
  return limits;
}

//A function that initializes the Best individual by allocating it the very first individual of the population
void
initialize_Best (int model, gsl_matrix * matrix, gsl_vector * Best, int n,
		 Params params, gsl_rng * r)
{
  //Retrieving the first individual of the population
  Solution variables;
  variables.M = gsl_matrix_get (matrix, 0, 0);
  variables.K = gsl_matrix_get (matrix, 0, 1);
  variables.Q = gsl_matrix_get (matrix, 0, 2);
  switch (model)
    {
      //In case of the first model the second variable is an integer
    case 1:
      variables.K = floor (variables.K);
      gsl_vector_set (Best, 1, variables.K);
      break;
    case 2:
    case 3:
    case 4:
      break;
      //The default case is the first model
    default:
      variables.K = floor (variables.K);
      gsl_vector_set (Best, 1, variables.K);
    }
  //Computing a very good approximation the fitness of the Best individual 
  objectif (model, &variables, 10000 * n, params, r);
  gsl_vector_set (Best, 0, variables.M);
  gsl_vector_set (Best, 1, variables.K);
  gsl_vector_set (Best, 2, variables.Q);
  gsl_vector_set (Best, 3, variables.fitness);
}

//A function for generating the first generation
void
initialization_pop (int model, gsl_matrix * matrix, Limits limits,
		    gsl_rng * r)
{
  //The size of the population; the number of individuals for each generation
  int size = matrix->size1;

  //Randomly generating an acceptable individual
  for (int i = 0; i < size; i++)
    {
      double buffer = 0;
      buffer =
	(limits.sup_M - limits.inf_M) * gsl_rng_uniform (r) + limits.inf_M;

      gsl_matrix_set (matrix, i, 0, buffer);

      //Verifiying which model is used
      switch (model)
	{
	case 1:
	  buffer = gsl_rng_uniform_int (r, 21);
	  break;
	case 2:
	case 3:
	case 4:
	  buffer =
	    (limits.sup_K - limits.inf_K) * gsl_rng_uniform (r) +
	    limits.inf_K;
	  break;
	default:
	  buffer = gsl_rng_uniform_int (r, 21);
	}
      gsl_matrix_set (matrix, i, 1, buffer);

      buffer =
	(limits.sup_Q - limits.inf_Q) * gsl_rng_uniform (r) + limits.inf_Q;
      gsl_matrix_set (matrix, i, 2, buffer);
    }
}

//A function that computes a low accuracy of the fitness of a given individual in a population
void
evaluation_individual (int model, gsl_matrix * matrix, int position, int n,
		       Params params, gsl_rng * r)
{
  Solution variables;
  variables.M = gsl_matrix_get (matrix, position, 0);
  variables.K = gsl_matrix_get (matrix, position, 1);
  variables.Q = gsl_matrix_get (matrix, position, 2);
  //Verifiying which model is used
  switch (model)
    {
    case 1:
      //Number of imperfect actions
      variables.K = floor (variables.K);
      gsl_matrix_set (matrix, position, 1, variables.K);
      break;
    case 2:
      //Threshold model
    case 3:
      //Approximated speed model
    case 4:
      //Exact speed model
      break;
    default:
      //The default case is the first model
      variables.K = floor (variables.K);
      gsl_matrix_set (matrix, position, 1, variables.K);
    }
  //Computing the fitness with a low accurracy
  objectif (model, &variables, n, params, r);
  gsl_matrix_set (matrix, position, 3, variables.fitness);
}

//A function that compares the fitness of an individual from the population with a another individual (most often the Best)
//The function returns a integer corresponding to the situation
int
comparison_individual (int model, gsl_matrix * matrix, int position,
		       gsl_vector * compared_with, int n, Params params,
		       gsl_rng * r)
{
  int a = 0;
  //We pour the individual in a buffer
  Solution variables;
  variables.M = gsl_matrix_get (matrix, position, 0);
  variables.K = gsl_matrix_get (matrix, position, 1);
  variables.Q = gsl_matrix_get (matrix, position, 2);
  //We first use the individual's low accuracy fitness to compare between the two
  //so to avoid huge calculations 
  if (gsl_matrix_get (matrix, position, 3) <
      gsl_vector_get (compared_with, 3))
    {

      //If the test is positive we compute a better accuracy of the fitness
      //Case 1: The compared individual passed at least one test but failed the final one
      a = 1;
      objectif (model, &variables, 100 * n, params, r);

      //We compare a second time the two individuals using a better accuracy so to validate the previous test
      if (gsl_vector_get (compared_with, 3) > variables.fitness)
	{
	  //If the test is positive we compute the best accuracy possible of the fitness
	  objectif (model, &variables, 10000 * n, params, r);
	  //We do a final test to validate previous results 
	  if (gsl_vector_get (compared_with, 3) > variables.fitness)
	    {
	      //If the individual passes the test comparing the best accuracy
	      //we consider it to perform better than the current Best
	      //so the former replaces the latter
	      //Case 2: The compared individual passes all the tests and is therefore the Best individual
	      a = 2;
	      gsl_vector *buffer = gsl_vector_calloc (4);
	      gsl_matrix_get_row (buffer, matrix, position);
	      gsl_vector_set (buffer, 3, variables.fitness);
	      gsl_vector_memcpy (compared_with, buffer);

	    }
	}
    }
  return a;
}

//The birth process of a new individual in the Diffrential Evolution function
void
new_born (gsl_matrix * parent_matrix, gsl_matrix * child_matrix,
	  Limits limits, double crossover, double diff_weight,
	  int position, gsl_rng * r)
{
  int size = parent_matrix->size1;
  //We keep a list of the already used individuals so to exclude them in next selections
  gsl_vector *exclude = gsl_vector_alloc (3);
  //Initialization of our list
  gsl_vector_set_all (exclude, -1);
  //We exlude the target individual
  gsl_vector_set (exclude, 0, position);
  int buffer = 0;

  //We select a first individual
  gsl_vector *a = gsl_vector_calloc (4);
  do
    {
      buffer = gsl_rng_uniform_int (r, size);
    }
  while (buffer == position);
  gsl_matrix_get_row (a, parent_matrix, buffer);
  gsl_vector_set (exclude, 1, buffer);


  //We select a second one
  gsl_vector *b = gsl_vector_calloc (4);
  do
    {
      buffer = gsl_rng_uniform_int (r, size);
    }
  while (buffer == position || buffer == gsl_vector_get (exclude, 1));
  gsl_matrix_get_row (b, parent_matrix, buffer);
  gsl_vector_set (exclude, 2, buffer);

  //We select a third one
  gsl_vector *c = gsl_vector_calloc (4);
  do
    {
      buffer = gsl_rng_uniform_int (r, size);
    }
  while (buffer == gsl_vector_get (exclude, 0)
	 || buffer == gsl_vector_get (exclude, 1)
	 || buffer == gsl_vector_get (exclude, 2));
  gsl_matrix_get_row (c, parent_matrix, buffer);

  //Arithmetic operations done on the three vectors to produce a mutant vector
  gsl_vector_sub (b, c);
  gsl_vector_scale (b, diff_weight);
  gsl_vector_add (a, b);

  //We make sure the mutant remains inside the boundaries of the search space
  adjust (a, limits);

  //We copy the target individual into vector b
  gsl_matrix_get_row (b, parent_matrix, position);

  //Crossover between the mutant and the target to produce a trial vector stored in a
  crossover_differential (a, b, crossover, r);

  //We select the best individual between the trial created from the crossover
  //and the target. This is the selection part of our Differential
  //Evolution algorithm
  double rand = gsl_rng_uniform (r);
  if ( gsl_vector_get(b, 3) < gsl_vector_get(a, 3) )
    gsl_vector_swap (a, b);

  gsl_matrix_set_row (child_matrix, position, a);

  gsl_vector_free (exclude);
  gsl_vector_free (a);
  gsl_vector_free (b);
  gsl_vector_free (c);
}


//The BLX-alpha crossover function used in the genetic function
void
genetic_crossover (int model, gsl_matrix * matrix, int position1,
		   int position2, double alpha, int n, Params params,
		   gsl_rng * r)
{   
    
    // We randomly generate from the 2 parents the values that will go into the offspring
    double generated_values[3];
    for (int i = 0; i < 3; i++) 
    {   
        // The ith components of the 1st and 2nd parents respectively
        double xi = gsl_matrix_get (matrix, position1, i);
        double yi = gsl_matrix_get (matrix, position2, i);
        // The distance between the two components
        double di = fabs(xi -yi);
        // The bounds of the support of the continuous uniform distribution 
        double lowerbound = fmin(xi, yi) - alpha*di;
        double upperbound = fmax(xi, yi) + alpha*di;
        // The uniform random variable
        double u = gsl_rng_uniform (r);
        generated_values[i] = lowerbound + abs(upperbound - lowerbound)*u;
    }
    
    // The generated offspring
    Solution offspring;
    offspring.M = generated_values[0];
    offspring.K = generated_values[1];
    offspring.Q = generated_values[2];
  
	//computation of the fitnesses of the new offspring
      objectif (model, &offspring, n, params, r);

      // The position of the replaced parent, if any
      int replaced_position = -1;
      //Replacement of the parents by the offspring
      // Which parent has the worst performance?
      if (gsl_matrix_get (matrix, position2, 3) < gsl_matrix_get (matrix, position1, 3))
          replaced_position = position1;
      else
          replaced_position = position2;
      // Does the offspring perform better than the worst parent?   
      // If yes replace that parent by the offspring
      if (offspring.fitness < gsl_matrix_get (matrix, replaced_position, 3))
      {
        gsl_matrix_set (matrix, replaced_position, 0, offspring.M);
        gsl_matrix_set (matrix, replaced_position, 1, offspring.K);
        gsl_matrix_set (matrix, replaced_position, 2, offspring.Q);
      }
	
    }

//A function that sets the positions of the two parents in the Genetic Algorithm
// A roulette wheel strategy to select the two parents
void
get_positions_for_crossover (gsl_matrix * matrix, int *position1,
			     int *position2, gsl_rng * r)
{
  int size = matrix->size1;

  //We compute the sum of all the fitnesses in the population
  double Sum = 0;
  for (int i_tr = 0; i_tr < size; i_tr++)
    {
      Sum = Sum + gsl_matrix_get (matrix, i_tr, 3);
    }

  //A vector that will save the partition of the interval [0, 1] 
  gsl_vector *TR = gsl_vector_calloc (size + 1);
  double S = 0;
  //We partition [0, Sum] into size intervals corresponding to the weight/priority we give to each individual
  for (int i_tr = 0; i_tr < size; i_tr++)
    {
      double buffer = gsl_matrix_get (matrix, i_tr, 3)/Sum;  
      S = S + (1 - buffer ) / (size - 1);
      gsl_vector_set (TR, i_tr + 1, S);
    }
    

  //We generate a uniformly distributed random number between 0 and 1
  double p1 = gsl_rng_uniform (r);
  int i_p = 0;
  *position1 = i_p;

  //We search for the individual whose interval contains the generated number
  while (i_p < size)
    {
      if (gsl_vector_get (TR, i_p) < p1 && p1 <= gsl_vector_get (TR, i_p + 1))
	{
	  *position1 = i_p;
	  i_p = size + 1;
	}
      else
	i_p++;
    }

  //We select a second parent by repeating the same procedure as before, this time excluding the first parent by jumping over its position (adding the length of its interval) after reaching its lower bound 

    //A boolean to check if we already jumped over the excluded interval
    int jump = 0;
    //We generate a second uniformly distributed random number between 0 and Sum
    double p2 = gsl_rng_uniform (r);
    i_p = 0;
    *position2 = i_p;
    while (i_p < size)
    {
        //We verify whether the generated number reached the lower_bound
        //of the excluded interval in order to jump over it
        if (!jump && i_p < gsl_vector_get (TR, *position1) )
            {
            //The jump
            p2 += gsl_vector_get (TR, *position1 + 1) - gsl_vector_get (TR, *position1);
            jump = 1;
            }
        if (gsl_vector_get (TR, i_p) < p2
            && p2 <= gsl_vector_get (TR, i_p + 1))
        {
            *position2 = i_p;
            i_p = size + 1;
        }
        else
        i_p++;
    }
}



//A function that executes the mutation part of the Genetic Algorithm
void
genetic_mutation (int model, gsl_matrix * matrix, Limits limits,
		  double mutation_rate, gsl_rng * r)
{
  int size = matrix->size1;
  for (int pos = 0; pos < size; pos++)
    {
      //Deciding whether or not to perform a mutation on the current individual,
      //with a probability mutation_rate of performing one
      double tm = gsl_rng_uniform (r);
      if (tm < mutation_rate)
	{
	  //Randomly choosing which variable to mutate
	  int POS = gsl_rng_uniform_int (r, 3);
	  double buffer;
	  if (POS == 0)
	    buffer = limits.sup_M * gsl_rng_uniform (r);
	  if (POS == 1)
	    {
	      //Verifiying which model is used
	      switch (model)
		{
		case 1:
		  //Number of imperfect actions
		  buffer = gsl_rng_uniform_int (r, 21);
		  break;
		case 2:
		  //Threshold model
		case 3:
		  //Approximated speed model
		case 4:
		  //Exact speed model
		  buffer =
		    (limits.sup_K - limits.inf_K) * gsl_rng_uniform (r) +
		    limits.inf_K;
		  break;
		default:
		  //The default case is the first model
		  buffer = gsl_rng_uniform_int (r, 21);
		}
	    }
	  if (POS == 2)
	    buffer =
	      (limits.sup_Q - limits.inf_Q) * gsl_rng_uniform (r) +
	      limits.inf_Q;

	  gsl_matrix_set (matrix, pos, POS, buffer);
	}
    }
}

//A function to displays a solution
void
display (int model, Solution variables)
{
  //Transforming the second variable into an integer in case we use the second model
  int K_prim = (int) variables.K;
  //Verifiying which model is used
  switch (model)
    {
    case 1:
      printf ("Number of imperfect actions model:\n");
      printf ("M = %.2lf\nK = %d\nQ = %.2lf\nFitness = %.2lf\n", variables.M,
	      K_prim, variables.Q, variables.fitness);
      break;
    case 2:
      printf ("Treshold model:\n");
      printf ("M = %.2lf\nK = %.2lf\nQ = %.2lf\nFitness = %.2lf\n",
	      variables.M, variables.K, variables.Q, variables.fitness);
      break;
    case 3:
      printf ("Estimated speed model:\n");
      printf ("M = %.2lf\nK = %.2lf\nQ = %.2lf\nFitness = %.2lf\n",
	      variables.M, variables.K, variables.Q, variables.fitness);
      break;
    case 4:
      printf ("Exact speed model:\n");
      printf ("M = %.2lf\nK = %.2lf\nQ = %.2lf\nFitness = %.2lf\n",
	      variables.M, variables.K, variables.Q, variables.fitness);
      break;
    default:
      printf ("Number of imperfect actions model:\n");
      printf ("M = %.2lf\nK = %d\nQ = %.2lf\nFitness = %.2lf\n", variables.M,
	      K_prim, variables.Q, variables.fitness);
    }
}

//A function that generates a random number according to a Gaussian distribution
//truncated on the interval [lower_bound, upper_bound]
double
trunc_gauss (double lower_bound, double upper_bound, gsl_rng * r)
{
  //The mean and variance of the distribution
  double mean = (upper_bound - lower_bound) / 2;
  double variance = (upper_bound - lower_bound) / 6;

  //Generating  a random number
  double U = gsl_rng_uniform (r);

  double alpha = (lower_bound - mean) / variance;
  double beta = (upper_bound - mean) / variance;

  //Computing the actual random number
  double rand_num =
    gsl_cdf_ugaussian_P (alpha) + U * (gsl_cdf_ugaussian_P (beta) -
				       gsl_cdf_ugaussian_P (alpha));
  rand_num = gsl_cdf_ugaussian_Pinv (rand_num);
  rand_num = rand_num * variance + mean;

  return rand_num;
}

//A function that returns the time to the next inspection time
double
m (double X, double Q, double alpha_k, Params params)
{
  int which = 3;
  double p = 1 - Q;
  double q = Q;
  double x = params.L - X;
  double shape = 1;
  double scale = params.beta;
  int statut;
  double bound;

  cdfgam (&which, &p, &q, &x, &shape, &scale, &statut, &bound);
  shape = shape / alpha_k;

  return shape;
}

//A function that simulates a cycle for the first model and returns the lenght H of the cycle
//plus the associated cost
double
cycle_number_imperfect (double M, int K, double Q, double *H, Params params,
			gsl_rng * r)
{
  //Eliminating forbidden values of Q; namely Q = 0 and Q = 0.01
  if (Q <= 0)
    Q = 0.01;
  else if (1 <= Q)
    Q = 0.99;

  //Initialiation
  //degradation level 
  double X = 0;
  //Inspection time
  double I = 0;
  //imperfect actions counter
  int k = 0;
  //degradation speed
  double speed = params.alpha0 / params.beta;
  //maintenance cost during the cycle
  double cost = 0;

  //Booleans to monitor whether we performed a corrective or a perfect action
  int corrective = 0;
  int perfect = 0;

  //We pursue the simulation until we perform a corrective or a perfect action
  while (corrective == 0 && perfect == 0)
    {
      double alpha_k = speed * params.beta;
      cost += params.Ci;
      double delta_I = m (X, Q, alpha_k, params);
      //Computing the next inspection time
      I += delta_I;
      double delta_X = gsl_ran_gamma (r, alpha_k * delta_I, 1 / params.beta);
      //Monitoring the degradation level of the system at next inspection time
      X += delta_X;
      //Has the system failed?
      if (X > params.L)
	{
	  //If yes we perfom a corrective maintenance; end of the cycle
	  corrective = 1;

	  //We compute the time the system has been down using the intercept theorem 
	  double down_time = ((X - params.L) * delta_I) / delta_X;
	  cost += params.Cc + params.Cd * down_time;
	}
      //Else we verify whether we perform any preventive maintenance
      else if (X > M)
	{
	  //If the test is positive
	  //We decide what kind of preventive maintenance to perform:
	  //Perfect or imperfect?
	  if (k < K)
	    {
	      //Imperfect preventive maintenance
	      //We simulate the improvement gain
	      double gain = trunc_gauss (0, X, r);
	      X -= gain;
	      //Incrementation of the imperfect actions counter
	      k++;
	      speed += gsl_ran_exponential (r, params.gamu);
	      cost += params.Cp0 * pow (gain / (X + gain), params.nu);
	    }
	  else
	    {
	      //Perfect preventive maintenance; end of the cycle
	      perfect = 1;
	      cost += params.Cp;
	    }
	}
    }

  //The cycle lenght receives the last inspection time
  *H = I;

  return cost;
}

//A function that simulates a cycle for the second model and returns the lenght H of the cycle
//plus the associated cost
double
cycle_seuil (double M, double T, double Q, double *H, Params params,
	     gsl_rng * r)
{
  //Eliminating forbidden values of Q; namely Q = 0 and Q = 0.01
  if (Q <= 0)
    Q = 0.01;
  else if (1 <= Q)
    Q = 0.99;

  //The second variable is a threshold and shouldn't be lower that the first variable
  if (T < M)
    {
      double buffer = T;
      T = M;
      M = buffer;
    }

  //Initialiation
  //degradation level 
  double X = 0;
  //Inspection time
  double I = 0;
  //degradation speed
  double speed = params.alpha0 / params.beta;
  //maintenance cost during the cycle
  double cost = 0;

  //Booleans to monitor whether we performed a corrective or a perfect action
  int corrective = 0;
  int perfect = 0;

  //We pursue the simulation until we perform a corrective or a perfect action
  while (corrective == 0 && perfect == 0)
    {
      double alpha_k = speed * params.beta;
      cost += params.Ci;
      double delta_I = m (X, Q, alpha_k, params);
      //Computing the next inspection time
      I += delta_I;
      double delta_X = gsl_ran_gamma (r, alpha_k * delta_I, 1 / params.beta);
      //Monitoring the degradation level of the system at next inspection time
      X += delta_X;
      //Has the system failed?
      if (X > params.L)
	{
	  //If yes we perfom a corrective maintenance; end of the cycle
	  corrective = 1;

	  //We compute the time the system has been down using a theorem 
	  double down_time = ((X - params.L) * delta_I) / delta_X;
	  cost += params.Cc + params.Cd * down_time;
	}
      //Else we verify whether we perform any preventive maintenance
      else if (X > M)
	{
	  //If the test is positive
	  //We decide what kind of preventive maintenance to perform:
	  //Perfect or imperfect?
	  if (X < T)
	    {
	      //Imperfect preventive maintenance
	      //We simulate the improvement gain
	      double gain = trunc_gauss (0, X, r);
	      X -= gain;
	      speed += gsl_ran_exponential (r, params.gamu);
	      cost += params.Cp0 * pow (gain / (X + gain), params.nu);
	    }
	  else
	    {
	      //Perfect preventive maintenance; end of the cycle
	      perfect = 1;
	      cost += params.Cp;
	    }
	}
    }

  //The cycle lenght receives the last inspection time
  *H = I;

  return cost;
}

//A function that simulates a cycle for the third model and returns the lenght H of the cycle
//plus the associated cost; the degradation speed is approximated
double
cycle_speed_estimate (double M, double S, double Q, double *H, Params params,
		      gsl_rng * r)
{
  //Eliminating forbidden values of Q; namely Q = 0 and Q = 0.01
  if (Q <= 0)
    Q = 0.01;
  else if (1 <= Q)
    Q = 0.99;

  //Initialiation
  //degradation level 
  double X = 0;
  //Inspection time
  double I = 0;
  //degradation speed
  double speed0 = params.alpha0 / params.beta;
  double speed = speed0;
  //maintenance cost during the cycle
  double cost = 0;

  //Booleans to monitor whether we performed a corrective or a perfect action
  int corrective = 0;
  int perfect = 0;

  //We pursue the simulation until we perform a corrective or a perfect action
  while (corrective == 0 && perfect == 0)
    {
      double alpha_k = speed * params.beta;
      cost += params.Ci;
      double delta_I = m (X, Q, alpha_k, params);
      //Computing the next inspection time
      I += delta_I;
      double delta_X = gsl_ran_gamma (r, alpha_k * delta_I, 1 / params.beta);
      //Monitoring the degradation level of the system at next inspection time
      X += delta_X;
      //We estimate the degradation speed using a simple formula since we don't have information on the real speed
      double speed_estimate = delta_X / delta_I;
      //Has the system failed?
      if (X > params.L)
	{
	  //If yes we perfom a corrective maintenance; end of the cycle
	  corrective = 1;

	  //We compute the time the system has been down using a theorem 
	  double down_time = ((X - params.L) * delta_I) / delta_X;
	  cost += params.Cc + params.Cd * down_time;
	}
      //Else we verify whether we perform any preventive maintenance
      else if (X > M)
	{
	  //If the test is positive
	  //We decide what kind of preventive maintenance to perform:
	  //Perfect or imperfect?
	  if (speed_estimate < speed0 + S)
	    {
	      //Imperfect preventive maintenance
	      //We simulate the improvement gain
	      double gain = trunc_gauss (0, X, r);
	      X -= gain;
	      speed += gsl_ran_exponential (r, params.gamu);
	      cost += params.Cp0 * pow (gain / (X + gain), params.nu);
	    }
	  else
	    {
	      //Perfect preventive maintenance; end of the cycle
	      perfect = 1;
	      cost += params.Cp;
	    }
	}
    }

  //The cycle lenght receives the last inspection time
  *H = I;

  return cost;
}

//A function that simulates a cycle for the third model and returns the lenght H of the cycle
//plus the associated cost; we assume that at any time we know the exact degration speed 
double
cycle_speed_exact (double M, double S, double Q, double *H, Params params,
		   gsl_rng * r)
{
  //Eliminating forbidden values of Q; namely Q = 0 and Q = 0.01
  if (Q <= 0)
    Q = 0.01;
  else if (1 <= Q)
    Q = 0.99;

  //Initialiation
  //degradation level 
  double X = 0;
  //Inspection time
  double I = 0;
  //degradation speed
  double speed0 = params.alpha0 / params.beta;
  double speed = speed0;
  //maintenance cost during the cycle
  double cost = 0;

  //Booleans to monitor whether we performed a corrective or a perfect action
  int corrective = 0;
  int perfect = 0;

  //We pursue the simulation until we perform a corrective or a perfect action
  while (corrective == 0 && perfect == 0)
    {
      double alpha_k = speed * params.beta;
      cost += params.Ci;
      double delta_I = m (X, Q, alpha_k, params);
      //Computing the next inspection time
      I += delta_I;
      double delta_X = gsl_ran_gamma (r, alpha_k * delta_I, 1 / params.beta);
      //Monitoring the degradation level of the system at next inspection time
      X += delta_X;
      //Has the system failed?
      if (X > params.L)
	{
	  //If yes we perfom a corrective maintenance; end of the cycle
	  corrective = 1;

	  //We compute the time the system has been down using a theorem 
	  double down_time = ((X - params.L) * delta_I) / delta_X;
	  cost += params.Cc + params.Cd * down_time;
	}
      //Else we verify whether we perform any preventive maintenance
      else if (X > M)
	{
	  //If the test is positive
	  //We decide what kind of preventive maintenance to perform:
	  //Perfect or imperfect?
	  if (speed < speed0 + S)
	    {
	      //Imperfect preventive maintenance
	      //We simulate the improvement gain
	      double gain = trunc_gauss (0, X, r);
	      X -= gain;
	      speed += gsl_ran_exponential (r, params.gamu);
	      cost += params.Cp0 * pow (gain / (X + gain), params.nu);
	    }
	  else
	    {
	      //Perfect preventive maintenance; end of the cycle
	      perfect = 1;
	      cost += params.Cp;
	    }
	}
    }

  //The cycle lenght receives the last inspection time
  *H = I;

  return cost;
}

//A function that computes/approximates the fitness (unit cost) 
//of an individual/solution using a Monte Carlo simulation
void
objectif (int model, Solution * variables, int n, Params params, gsl_rng * r)
{
  //Transforming the second variable into an integer in case we use the second model
  int K_prim = (int) variables->K;

  double sumC = 0;
  double sumH = 0;

  //Initialization
  switch (model)
    {
    case 1:
      //Initialization
      for (int i = 0; i < n; i++)
	{
	  double pointer;
	  double cost =
	    cycle_number_imperfect (variables->M, K_prim, variables->Q,
				    &pointer, params, r);
	  sumC += cost;
	  sumH += pointer;
	}
      break;
    case 2:
      //Number of imperfect actions
      for (int i = 0; i < n; i++)
	{
	  double pointer;
	  double cost =
	    cycle_seuil (variables->M, variables->K, variables->Q, &pointer,
			 params, r);
	  sumC += cost;
	  sumH += pointer;
	}
      break;

    case 3:
      //Approximated speed model
      for (int i = 0; i < n; i++)
	{
	  double pointer;
	  double cost =
	    cycle_speed_estimate (variables->M, variables->K, variables->Q,
				  &pointer, params, r);
	  sumC += cost;
	  sumH += pointer;
	}
      break;


    case 4:
      //Exact speed model
      for (int i = 0; i < n; i++)
	{
	  double pointer;
	  double cost =
	    cycle_speed_exact (variables->M, variables->K, variables->Q,
			       &pointer, params, r);
	  sumC += cost;
	  sumH += pointer;
	}

      break;
    default:
      //The default case is the first model
      for (int i = 0; i < n; i++)
	{
	  double pointer;
	  double cost =
	    cycle_number_imperfect (variables->M, K_prim, variables->Q,
				    &pointer, params, r);
	  sumC += cost;
	  sumH += pointer;
	}

    }

  variables->fitness = sumC / sumH;
}

//A function that readjusts an infeasible solution into a feasible one
//by reorienting the out-of-bounds components inside the boundaries
//of the search space
void
adjust (gsl_vector * v, Limits limits)
{
  double M = gsl_vector_get (v, 0);
  double K = gsl_vector_get (v, 1);
  double Q = gsl_vector_get (v, 2);

  if (M < limits.inf_M)
    M = limits.inf_M - M;
  else if (limits.sup_M < M)
    M = 2 * limits.sup_M - M;

  if (K < limits.inf_K)
    K = limits.inf_K - K;
  else if (limits.sup_K < K)
    K = 2 * limits.sup_K - K;

  if (Q < limits.inf_Q)
    Q = limits.inf_Q - Q;
  else if (limits.sup_Q < Q)
    Q = 2 * limits.sup_Q - Q;

  gsl_vector_set (v, 0, M);
  gsl_vector_set (v, 1, K);
  gsl_vector_set (v, 2, Q);
}

//A function that crosses a mutant and a target vectors to produce a trial vector for the differential evolution algorithm
//The trial vector will be saved in mutant and we will ensure that at least one component of mutant will get into trial
void
crossover_differential (gsl_vector * mutant, gsl_vector * target, double CR, gsl_rng * r)
{
  int size = mutant->size;
  // R will be the unchanged position of the mutant vector
  int R = gsl_rng_uniform_int(r, size);
  
  //We go through all the components of the vectors
  for (int i = 0; i < size; i++)
    {
      double rand = gsl_rng_uniform (r);
      // If the generated random number is larger than CR the ith component of trial will receive the value in target 
      // i != R : At least one component of mutant will get into trial
      if (rand > CR && i != R)
	{
	  double target_i = gsl_vector_get (target, i);

	  gsl_vector_set (mutant, i, target_i);
	}
    }
}

//An implementation of a genetic algorithm in order to search for the best solution (decision variables)
//for a given model and parameters
void
genetic (int model, int n, int pop_size, int nb_gen, double blend_parameter,
	 double mutation_rate, Params params, gsl_rng * r, Solution * optimum, int * statistics)
{
  //We keep track of the number of evaluations
  int nb_eval = 0;

  if (pop_size < 1)
    pop_size = 10;

  Limits limits = set_limits (model, params.L);

  //each row of the pop matrix represents an individual
  gsl_matrix *pop = gsl_matrix_calloc (pop_size, 4);
  gsl_matrix *popQ = gsl_matrix_calloc (pop_size, 4);


  initialization_pop (model, pop, limits, r);

  //The Best solution found so far
  gsl_vector *Best = gsl_vector_calloc (4);
  initialize_Best (model, pop, Best, n, params, r);

  //For informative purposes we monitor the execution time
  clock_t t1 = clock ();
  clock_t t2 = t1;
  double elapsed_time = 0;

  //For informative purposes we monitor the last time a best solution was almost found
  //and the last time a best solution was found
  int nb_evaluated_individuals = 0;
  int nb_since_almost_best = 0;
  int nb_since_best = 0;

  //We run the algorithm for a certain number of generations
  for (int i_g = 0; i_g < nb_gen; i_g++)
    {
      //For every individual in the current generation we compute its fitness and compare it to the Best
      for (int i = 0; i < pop_size; i++)
	{
	  evaluation_individual (model, pop, i, n, params, r);
	  nb_evaluated_individuals++;
	  nb_since_best++;
	  nb_since_almost_best++;

	  //We verify whether a Best solution was found or not
	  int a = comparison_individual (model, pop, i, Best, n, params, r);

	  if (a == 1)
	    nb_since_almost_best = 0;
	  else if (a == 2)
	    nb_since_best = 0;
	}

      int number_crossovers = gsl_rng_uniform_int (r, pop_size);
      for (int i_nc = 0; i_nc < number_crossovers; i_nc++)
	{
	  int pos1;
	  int pos2;
      
      // A roulette wheel strategy to select the two parents 
	  get_positions_for_crossover (pop, &pos1, &pos2, r);

	  genetic_crossover (model, pop, pos1, pos2, blend_parameter, n,
			     params, r);
	}
      genetic_mutation (model, pop, limits, mutation_rate, r);
    }
  t2 = clock ();
  elapsed_time = (t2 - t1) / CLOCKS_PER_SEC;  
  
  //Saving the optimal solution
  optimum->M = gsl_vector_get (Best, 0);
  optimum->K = gsl_vector_get (Best, 1);
  optimum->Q = gsl_vector_get (Best, 2);

  optimum->fitness = gsl_vector_get (Best, 3);

  nb_eval = nb_evaluated_individuals;
  if(statistics != NULL)
  {
  // We fill in the statistics
  statistics[0] = nb_evaluated_individuals;
  statistics[1] = nb_since_almost_best;
  statistics[2] = nb_since_best;
  statistics[3] = nb_eval;
  statistics[4] = (int) elapsed_time;
  } 
  gsl_matrix_free (pop);
  gsl_matrix_free (popQ);
  gsl_vector_free (Best);
}

//An implementation of a differential evolution metaheuristic in order to search for the best solution (decision variables)
//for a given model and parameters
void
diff_Evol (int model, int n, int pop_size, int nb_gen, double crossover,
	   double diff_weight, Params params, gsl_rng * r,
	   Solution * optimum, int *statistics)
{
  //We keep track of the number of evaluations
  int nb_eval = 0;

  //The differential weight needs not to exceed [0.5, 2]
  if (diff_weight <= 0.5 || 2 <= diff_weight)
    diff_weight = 0.8;

  if (pop_size < 1)
    pop_size = 10;

  Limits limits = set_limits (model, params.L);

  //each row of the pop matrix represents an individual
  gsl_matrix *pop = gsl_matrix_calloc (pop_size, 4);
  gsl_matrix *pop_children = gsl_matrix_calloc (pop_size, 4);

  initialization_pop (model, pop, limits, r);

  //The Best solution found so far
  gsl_vector *Best = gsl_vector_calloc (4);
  initialize_Best (model, pop, Best, n, params, r);

  //For informative purposes we monitor the execution time
  clock_t t1 = clock ();
  clock_t t2 = t1;
  double elapsed_time = 0;

  //For informative purposes we monitor the last time a best solution was almost found
  //and the last time a best solution was found
  int nb_evaluated_individuals = 0;
  int nb_since_almost_best = 0;
  int nb_since_best = 0;

  //We run the algorithm for a certain number of generations
  for (int i_g = 0; i_g < nb_gen; i_g++)
    {
      //For every individual in the current generation we compute its fitness and compare it to the Best
      for (int i = 0; i < pop_size; i++)
	{
	  evaluation_individual (model, pop, i, n, params, r);
	  nb_evaluated_individuals++;
	  nb_since_best++;
	  nb_since_almost_best++;

	  //We verify whether a Best solution was found or not
	  int a = comparison_individual (model, pop, i, Best, n, params, r);

	  if (a == 1)
	    nb_since_almost_best = 0;
	  else if (a == 2)
	    nb_since_best = 0;
	}

      gsl_matrix_memcpy (pop_children, pop);

      for (int i = 0; i < pop_size; i++)
        new_born (pop, pop_children, limits, crossover, diff_weight, i, r);
      

      //After some trials it appeared that completely replacing the old generation
      //by the new one yields a faster convergence to the optimal solution than
      //selecting the best children to replace their parents; the selection part of
      //our algorithm consists of the selecting the best child among the two 
      //generated by the crossover in new_born
      
      gsl_matrix_memcpy (pop, pop_children);
    }

  t2 = clock ();
  elapsed_time = (t2 - t1) / CLOCKS_PER_SEC;


  //Saving the optimal solution
  optimum->M = gsl_vector_get (Best, 0);
  optimum->K = gsl_vector_get (Best, 1);
  optimum->Q = gsl_vector_get (Best, 2);

  optimum->fitness = gsl_vector_get (Best, 3);

  nb_eval = nb_evaluated_individuals;
  if(statistics != NULL)
  {
  // We fill in the statistics
  statistics[0] = nb_evaluated_individuals;
  statistics[1] = nb_since_almost_best;
  statistics[2] = nb_since_best;
  statistics[3] = nb_eval;
  statistics[4] = (int) elapsed_time;
  } 

  gsl_matrix_free (pop);
  gsl_matrix_free (pop_children);
  gsl_vector_free (Best);
}

//Parameters

gamu = 0.2;
nu = 3;
L = 20;
Ci = 10;
Cc = 100;
Cp = 90;
Cp0 = 70;
Cd = 20;

alpha0 = 1;
BETA = 1;

v0 = alpha0/BETA;

//A function that generates random numbers according to a truncated gaussian distribution
function x = trunc_gauss(a, b)
    mean2 = (b-a)/2;
    var = (b-a)/6;
    U = grand(1, 1,"def");

    alpha = (a - mean2)/var;
    beta2 = (b - mean2)/var;

    buffer = cdfnor("PQ", alpha, 0, 1) + U*(cdfnor("PQ", beta2, 0, 1) - cdfnor("PQ", alpha, 0, 1) );

    x = cdfnor("X", 0, 1, buffer, 1-buffer);

    x = x*var + mean2;

endfunction

//A function that simulates a cycle for the number of imperfect actions model
function [H, cost] = cycle1(M, K, Q)

    if(Q == 0) then
        Q = 0.01;
    elseif (Q == 1) then
        Q = 9.99
    end

    corrective = 0;
    perfect = 0;

    X = [0];
    T = [0];
    vk = alpha0/BETA;
    cost = 0;

    k =0;
    i = 1;

    cost = 0;

    while (corrective == 0) & (perfect == 0) 
        i = i + 1;
        alphak = vk*BETA;
        cost = cost + Ci;
        m = cdfgam("Shape", BETA, 1 - Q, Q, L - X(i-1))/alphak;
        T(i) = T(i-1) + m;
        X(i) = X(i-1) + grand(1, 1, "gam", alphak*abs(T(i) - T(i-1)), BETA);
        if (X(i) > L) then
            // Computing down time
            r1 = X(i) - L;
            r2 = X(i) - X(i-1);
            s2 = T(i) - T(i-1);
            s1 = r1*s2/r2;
            vk = alpha0/BETA;
            k = 0;

            cost = cost + Cc + Cd*s1;

            corrective = 1;

        elseif (X(i) > M) then
            if (k < K) then 
                Z = trunc_gauss(0, X(i));
                i = i+1;
                X(i) = X(i-1) - Z;
                T(i) = T(i-1);
                k = k + 1;
                vk = vk + grand(1, 1, "exp", gamu);
                cost = cost + Cp0*(Z/X(i-1))**nu;
            else 
                i = i+1;
                T(i) = T(i-1);
                X(i) = 0;
                k = 0;
                vk = alpha0/BETA;
                perfect = 1;
                cost = cost + Cp;
            end
        end

        H = T(i);

    end

endfunction

//A function that simulates a cycle for the threshold model
function [H, cost, V, delta] = cycle2(M, S, Q)

    if(Q == 0) then
        Q = 0.01;
    elseif (Q == 1) then
        Q = 9.99
    end

    if(S<M)
        P = S;
        S = M;
        M = P;
    end
    corrective = 0;
    perfect = 0;

    X = [0];
    T = [0];
    vk = alpha0/BETA;
    V(1) = vk;
    i = 1;
    j = 1;
    cost = 0;

    while (corrective == 0) & (perfect == 0) 
        i = i + 1;
        alphak = vk*BETA;
        cost = cost + Ci;
        m = cdfgam("Shape", BETA, 1 - Q, Q, L - X(i-1))/alphak;
        T(i) = T(i-1) + m;
        X(i) = X(i-1) + grand(1, 1, "gam", alphak*abs(T(i) - T(i-1)), BETA);


        if (X(i) > L) then
            // Computing down time
            r1 = X(i) - L;
            r2 = X(i) - X(i-1);
            s2 = T(i) - T(i-1);
            s1 = r1*s2/r2;

            vk = alpha0/BETA;
            V(j) = vk;
            cost = cost + Cc + Cd*s1;
            corrective = 1;

        elseif (X(i) > M) then
            if (X(i) < S) then 
                Z = trunc_gauss(0, X(i));
                i = i+1;
                X(i) = X(i-1) - Z;
                T(i) = T(i-1);
                vk = vk + grand(1, 1, "exp", gamu);
                V(j) = vk;
                j = j+1;
                cost = cost + Cp0*(Z/X(i-1))**nu;
            else 
                i = i+1;
                T(i) = T(i-1);
                X(i) = 0;
                vk = alpha0/BETA;
                V(j) = vk;
                j = j+1;
                cost = cost + Cp;
                perfect = 1;
            end
        end
    end

    H = T(i);
endfunction

//A function that simulates a cycle for the approximated speed model
function [H, cost] = cycle3(M, K, Q)

    if(Q == 0) then
        Q = 0.01;
    elseif (Q == 1) then
        Q = 0.99;
    end

    corrective = 0;
    perfect = 0;

    X = [0];
    T = [0];
    vk = alpha0/BETA;

    i = 1;
    cost = 0;

    while (corrective == 0) & (perfect == 0) 
        i = i + 1;
        alphak = vk*BETA;
        cost = cost + Ci;
        m = cdfgam("Shape", BETA, 1 - Q, Q, L - X(i-1))/alphak;
        T(i) = T(i-1) + m;
        X(i) = X(i-1) + grand(1, 1, "gam", alphak*abs(T(i) - T(i-1)), BETA);

        //Approximating the degradation speed
        V = X(i) - X(i-1);
        V = V/(T(i) - T(i-1));

        if (X(i) > L) then
            // Computing down time
            r1 = X(i) - L;
            r2 = X(i) - X(i-1);
            s2 = T(i) - T(i-1);
            s1 = r1*s2/r2;
            vk = alpha0/BETA;

            cost = cost + Cc + Cd*s1;

            corrective = 1;

        elseif (X(i) > M) then
            if (V < v0 + K) then 
                Z = trunc_gauss(0, X(i));
                i = i+1;
                X(i) = X(i-1) - Z;
                T(i) = T(i-1);
                vk = vk + grand(1, 1, "exp", gamu);
                cost = cost + Cp0*(Z/X(i-1))**nu;
            else 
                i = i+1;
                T(i) = T(i-1);
                X(i) = 0;
                vk = alpha0/BETA;
                perfect = 1;
                cost = cost + Cp;
            end
        end
    end

    H = T(i);

endfunction

//A function that simulates a cycle for the exact speed model
function [H, cost] = cycle4(M, K, Q)

    if(Q == 0) then
        Q = 0.01;
    elseif(Q == 1) then
        Q = 0.99
    end

    corrective = 0;
    perfect = 0;

    X = [0];
    T = [0];
    vk = alpha0/BETA;

    i = 1;
    cost = 0;

    while (corrective == 0) & (perfect == 0) 
        i = i + 1;
        alphak = vk*BETA;
        cost = cost + Ci;
        m = cdfgam("Shape", BETA, 1 - Q, Q, L - X(i-1))/alphak;
        T(i) = T(i-1) + m;
        X(i) = X(i-1) + grand(1, 1, "gam", alphak*abs(T(i) - T(i-1)), BETA);

        if (X(i) > L) then
            // Computing down time
            r1 = X(i) - L;
            r2 = X(i) - X(i-1);
            s2 = T(i) - T(i-1);
            s1 = r1*s2/r2;
            vk = alpha0/BETA;

            cost = cost + Cc + Cd*s1;

            corrective = 1;

        elseif (X(i) > M) then
            if (vk < v0 + K) then 
                Z = trunc_gauss(0, X(i));
                i = i+1;
                X(i) = X(i-1) - Z;
                T(i) = T(i-1);
                vk = vk + grand(1, 1, "exp", gamu);
                cost = cost + Cp0*(Z/X(i-1))**nu;
            else 
                i = i+1;
                T(i) = T(i-1);
                X(i) = 0;
                //k = 0;
                vk = alpha0/BETA;
                perfect = 1;
                cost = cost + Cp;
            end
        end
    end

    H = T(i);
endfunction

//Fonction objectif
function Z = objectif (model, M, K, Q, n)
    for nb = 1:n
        //We simulates a cycle corresponding to one of the models
        select model
        case 1
            [H(nb), cost(nb)] = cycle1(M, K, Q);
        case 2
            [H(nb), cost(nb)] = cycle2(M, K, Q);
        case 3
            [H(nb), cost(nb)] = cycle3(M, K, Q);
        case 4
            [H(nb), cost(nb)] = cycle4(M, K, Q);
        else
            //The default is the first model
            [H(nb), cost(nb)] = cycle1(M, K, Q);
        end
    end

    Z = sum(cost)/sum(H);
endfunction


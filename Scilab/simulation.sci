//Parameters

gamu = 0.2;
nu = 3;
L = 20;
Ci = 10;
Cc = 100;
Cp = 90;
Cp0 = 70;
Cd = 20;

alpha0 = 1;
BETA = 1;

v0 = alpha0/BETA;

//A function that generates random numbers according to a truncated gaussian distribution
function x = trunc_gauss(a, b)
    mean2 = (b-a)/2;
    var = (b-a)/6;
    U = grand(1, 1,"def");

    alpha = (a - mean2)/var;
    beta2 = (b - mean2)/var;

    buffer = cdfnor("PQ", alpha, 0, 1) + U*(cdfnor("PQ", beta2, 0, 1) - cdfnor("PQ", alpha, 0, 1) );

    x = cdfnor("X", 0, 1, buffer, 1-buffer);

    x = x*var + mean2;

endfunction

//A function that simulates a life cycle for the number of imperfect actions model
function cost = simulation1(M, K, Q, horizon)

    if(Q == 0) then
        Q = 0.01;
    elseif (Q == 1) then
        Q = 9.99
    end

    X = [0];
    T = [0];
    vk = alpha0/BETA;
    cost = 0;

    k =0;
    i = 1;

    cost = 0;

    while (T(i) < horizon)
        i = i + 1;
        alphak = vk*BETA;
        cost = cost + Ci;
        m = cdfgam("Shape", BETA, 1 - Q, Q, L - X(i-1))/alphak;
        T(i) = T(i-1) + m;
        X(i) = X(i-1) + grand(1, 1, "gam", alphak*abs(T(i) - T(i-1)), BETA);
        if (X(i) > L) then
            // Computing down time
            r1 = X(i) - L;
            r2 = X(i) - X(i-1);
            s2 = T(i) - T(i-1);
            s1 = r1*s2/r2;
            vk = alpha0/BETA;
            i = i+1;
            X(i) = 0;
            T(i) = T(i-1);
            k = k +1;
            cost = cost + Cc + Cd*s1;

        elseif (X(i) > M) then
            if (k < K) then 
                Z = trunc_gauss(0, X(i));
                i = i+1;
                X(i) = X(i-1) - Z;
                T(i) = T(i-1);
                k = k + 1;
                vk = vk + grand(1, 1, "exp", gamu);
                cost = cost + Cp0*(Z/X(i-1))**nu;
            else 
                i = i+1;
                T(i) = T(i-1);
                X(i) = 0;
                k = 0;
                vk = alpha0/BETA;
                cost = cost + Cp;
            end
        end
    end

    //plotting
    clf();
    plot([0, T(i)], [L, L], "r");
    plot([0, T(i)], [M, M], "g");
    plot(T, X);
    legend('failure threshold L', 'maintenance threshold M', 'Degradation curve X(t)');
    xtitle("Simulation of the life cycte of a system", "time t", "Degradation level X");

endfunction

//A function that simulates a life cycle for the threshold model
function cost = simulation2(M, Tr, Q, horizon)

    if(Q == 0) then
        Q = 0.01;
    elseif (Q == 1) then
        Q = 9.99
    end

    if(Tr<M)
        P = Tr;
        Tr = M;
        M = P;
    end

    X = [0];
    T = [0];
    vk = alpha0/BETA;
    cost = 0;

    i = 1;

    cost = 0;

    while (T(i) < horizon)
        i = i + 1;
        alphak = vk*BETA;
        cost = cost + Ci;
        m = cdfgam("Shape", BETA, 1 - Q, Q, L - X(i-1))/alphak;
        T(i) = T(i-1) + m;
        X(i) = X(i-1) + grand(1, 1, "gam", alphak*abs(T(i) - T(i-1)), BETA);
        if (X(i) > L) then
            // Computing down time
            r1 = X(i) - L;
            r2 = X(i) - X(i-1);
            s2 = T(i) - T(i-1);
            s1 = r1*s2/r2;
            vk = alpha0/BETA;
            i = i+1;
            X(i) = 0;
            T(i) = T(i-1);

            cost = cost + Cc + Cd*s1;

        elseif (X(i) > M) then
            if (X(i) < Tr) then 
                Z = trunc_gauss(0, X(i));
                i = i+1;
                X(i) = X(i-1) - Z;
                T(i) = T(i-1);
                vk = vk + grand(1, 1, "exp", gamu);
                cost = cost + Cp0*(Z/X(i-1))**nu;
            else 
                i = i+1;
                T(i) = T(i-1);
                X(i) = 0;
                vk = alpha0/BETA;
                cost = cost + Cp;
            end
        end

    end

    //plotting
    clf();
    plot([0, T(i)], [L, L], "r");
    plot([0, T(i)], [M, M], "g");
    plot([0, T(i)], [Tr, Tr], "bk");
    plot(T, X);
    legend('failure threshold L', 'maintenance threshold M', 'perfect maintenance threshold Tr', 'Degradation curve X(t)');
    xtitle("Simulation of the life cycte of a system", "time t", "Degradation level X");

endfunction

//A function that simulates a life cycle for the approximated speed model
function cost = simulation3(M, S, Q, horizon)

    if(Q == 0) then
        Q = 0.01;
    elseif (Q == 1) then
        Q = 9.99
    end

    X = [0];
    T = [0];
    vk = alpha0/BETA;
    cost = 0;

    i = 1;

    cost = 0;

    while (T(i) < horizon)
        i = i + 1;
        alphak = vk*BETA;
        cost = cost + Ci;
        m = cdfgam("Shape", BETA, 1 - Q, Q, L - X(i-1))/alphak;
        T(i) = T(i-1) + m;
        X(i) = X(i-1) + grand(1, 1, "gam", alphak*abs(T(i) - T(i-1)), BETA);

        //Approximating the degradation speed
        V = X(i) - X(i-1);
        V = V/(T(i) - T(i-1));

        if (X(i) > L) then
            // Computing down time
            r1 = X(i) - L;
            r2 = X(i) - X(i-1);
            s2 = T(i) - T(i-1);
            s1 = r1*s2/r2;
            vk = alpha0/BETA;
            i = i+1;
            X(i) = 0;
            T(i) = T(i-1);

            cost = cost + Cc + Cd*s1;

        elseif (X(i) > M) then
            if (V < v0 + S) then 
                Z = trunc_gauss(0, X(i));
                i = i+1;
                X(i) = X(i-1) - Z;
                T(i) = T(i-1);
                vk = vk + grand(1, 1, "exp", gamu);
                cost = cost + Cp0*(Z/X(i-1))**nu;
            else 
                i = i+1;
                T(i) = T(i-1);
                X(i) = 0;
                vk = alpha0/BETA;
                cost = cost + Cp;
            end
        end

    end

    //plotting
    clf();
    plot([0, T(i)], [L, L], "r");
    plot([0, T(i)], [M, M], "g");
    plot(T, X);
    legend('failure threshold L', 'maintenance threshold M', 'Degradation curve X(t)');
    xtitle("Simulation of the life cycte of a system", "time t", "Degradation level X");

endfunction

//A function that simulates a life cycle for the exact speed model
function cost = simulation4(M, S, Q, horizon)

    if(Q == 0) then
        Q = 0.01;
    elseif (Q == 1) then
        Q = 9.99
    end

    X = [0];
    T = [0];
    vk = alpha0/BETA;
    cost = 0;

    i = 1;

    cost = 0;

    while (T(i) < horizon)
        i = i + 1;
        alphak = vk*BETA;
        cost = cost + Ci;
        m = cdfgam("Shape", BETA, 1 - Q, Q, L - X(i-1))/alphak;
        T(i) = T(i-1) + m;
        X(i) = X(i-1) + grand(1, 1, "gam", alphak*abs(T(i) - T(i-1)), BETA);

        if (X(i) > L) then
            // Computing down time
            r1 = X(i) - L;
            r2 = X(i) - X(i-1);
            s2 = T(i) - T(i-1);
            s1 = r1*s2/r2;
            vk = alpha0/BETA;
            i = i+1;
            X(i) = 0;
            T(i) = T(i-1);

            cost = cost + Cc + Cd*s1;

        elseif (X(i) > M) then
            if (vk < v0 + S) then 
                Z = trunc_gauss(0, X(i));
                i = i+1;
                X(i) = X(i-1) - Z;
                T(i) = T(i-1);
                vk = vk + grand(1, 1, "exp", gamu);
                cost = cost + Cp0*(Z/X(i-1))**nu;
            else 
                i = i+1;
                T(i) = T(i-1);
                X(i) = 0;
                vk = alpha0/BETA;
                cost = cost + Cp;
            end
        end

    end

    //plotting
    clf();
    plot([0, T(i)], [L, L], "r");
    plot([0, T(i)], [M, M], "g");
    plot(T, X);
    legend('failure threshold L', 'maintenance threshold M', 'Degradation curve X(t)');
    xtitle("Simulation of the life cycte of a system", "time t", "Degradation level X");
endfunction

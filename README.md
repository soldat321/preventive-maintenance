 ### Copyright

    Copyright (C) 2019 Abdellaziz Mohamed Amine, Zaoui Riyadh

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or (at
    your option) any later version.
 
    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.
 
    You should have received a copy of the GNU General Public License 
    along with this program.  If not, see https://www.gnu.org/licenses/.
 
 ### Introduction
 
 This is the source code of our Final Year Project for our master's degree in 
 Operations Research at the USTHB, Algiers.
 
 To learn more about the project and understand the purpose of the programs 
 please visit [my website](https://abdellazizamine.wordpress.com/projects/preventive-maintenance/).
 
 For the _Cumulative Density Functions_ part we have used a library written by
 Barry Brown, James Lovato and Kathy Russell from University of Texas,
Houston, Texas. For more information you can go to their [webpage](https://people.sc.fsu.edu/~jburkardt/c_src/cdflib/cdflib.html).

 ### Make it work


 #### Scilab 

 In the _./Scilab_ directory there are two scilab scripts, _models.sci_ which contains the functions that simulates a single cycle for the three studied models ans _simulation.sci_ that simulates a whole life cycle for the models (it uses functions from the previous script for that matter). To use the scripts simply run Scilab and load the files. Report yourself to the source code and the memoir for the details.

 #### C

 To find the source code of the C programs go to _./C/source/_. There are five programs, each one contained in a separate directory. To compile a program just execute
 
 `$ make`

 inside the associated dirctory and you will find the executable in _./C_. To delete the generated object files use 

 `$ make clear`

 and to delete the executable along the unwanted object files type

 `$ make clean`

 You can find _.params_ (parameters) files in the _./C/parameters_ folder or create your own by writing the values in a file following this exact order 

 `alpha beta gamma nu L Ci Cc Cp Cp^0 Cd`

 Finally the folder *./C/comparison_results* contains the results found during our executions of *comparison_methods* and *comparison_models*. Because those two programs write the resutas on the standard output it is advisable to redirect the flow to a file like this

 `$ ./*comparison_methods* >> results.tex`

 The results are written in a way to be incorporated in a LaTeX file (whose header file can be found in *./C/compariosn_results*) and be compiled to get a nice table.



